---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west headline text-white span-100]
Défi Zéro Déchets<br>*Vers la transition numérique*
@snapend

@snap[south-west byline  text-white]
Atelier proposé par François-Xavier Guillois
@snapend

---?include=assets/md/introduction/Introduction.md

---?include=assets/md/outils/outils.md

---?include=assets/md/outils/appareils.md

---?include=assets/md/outils/bilan-energetique.md

---?include=assets/md/outils/cloud.md

---?include=assets/md/outils/noyau-familial.md

---?include=assets/md/ecologie/ecologie.md

---?include=assets/md/ecologie/impact.md

---?include=assets/md/ecologie/trash.md

---?include=assets/md/ecologie/consommer-moins.md

---?include=assets/md/ecologie/consommer-local.md

---?include=assets/md/securite/securite.md

---?include=assets/md/securite/bonnes-pratiques.md

---?include=assets/md/securite/mon-navigateur.md

---?include=assets/md/securite/sauvegarder-en-securite.md

---?include=assets/md/securite/objets-connectes.md

---?include=assets/md/conclusion/conclusion.md

---?include=assets/md/conclusion/questions.md

---?include=assets/md/conclusion/contact.md
