@title[Get In Touch #3]

@snap[west contact-links]
@css[contact-name](François-Xavier Guillois)<br>
<a href="https://twitter.com/tnntwister">
@fa[twitter-square pad-right-icon]@css[twitter-handle](@tnntwister)
</a><br>
<a href="https://bitbucket.org/tnntwister/transition-numerique/">
@fa[bitbucket-square pad-right-icon]@css[git-handle](tnntwister)
</a><br>
<a href="mailto: francoisxavier.guillois@gmail.com">
@fa[envelope-o pad-right-icon]@css[contact-email](francoisxavier.guillois@gmail.com)
</a>
@snapend

@snap[east span-50]
![](template/img/contact-1.png)
@snapend

@snap[north-east template-note text-gray]
N'hésitez pas à me contacter après l'atelier !
@snapend