---?image=assets/img/bg/orange.jpg&position=right&size=60% 100%
@title[conclusion]

@snap[west split-screen-heading text-orange span-40]
conclusion
@snapend
@snap[east text-white span-60]
@ol[split-screen-list](false)
- passation : aider les jeunes à trouver leurs repères
- crises systémiques : Servigne, Bihouix, Jancovici...
- une résilience géographique à créer ([sos maires](https://sosmaires.org/), [villes en transition](http://villesentransition.grenoble.fr/))
- comme un régime alimentaire : doucement pour + longtemps  
@olend
@snapend
