---?image=assets/img/pencils.jpg
@title[outils]

## @color[black](les outils)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[outils]

@snap[west split-screen-heading text-orange span-50]
Les outils
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Connaître ses appareils
- Optimiser son bilan énergétique
- Cloud ou pas cloud ?
- Créer un noyau familial
@olend
@snapend
