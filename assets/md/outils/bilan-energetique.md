---?image=assets/img/pencils.jpg
@title[optimiser son bilan énergétique]

## @color[black](Optimiser son bilan énergétique)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=64% 100%
@title[les conclusions de l'observation]

@snap[west span-40 text-orange]
![SNOWMAN](assets/img/data/comparaison-client-leger-desktop.png)
@snapend

@snap[east span-60]
@ul[split-screen-list text-white](false)
- Le réseau consomme quoiqu'il arrive = mutualisation des ressources
- En abaissant notre consommation de ressources, nous devenons plus résilients
- En utilisant des machines d'occasion, nous abaissons encore notre consommation
- On passe d'une logique individuelle (grosse voiture) à une logique d'échanges (transports en commmun)
@ulend
@snapend

@snap[south-west template-note text-gray]
Les conclusions de l'observation des appareils 
@snapend

+++?image=assets/img/bg/orange.jpg
@title[optimisation dans les entreprises]
@ul[split-screen-list text-white](false)
- 80% des enteprises ne savent pas quel % représente la consommation électrique de l’informatique  en regard de la consommation électrique globale de leur organisation.
- Pour plus de 66% d'entre elles, il n’est pas certain que la demande de puissance soit dimensionnée en fonction des besoins de la salle serveur.
- Moins de 10 % utilisent des onduleurs à volant d’inertie (haut rendement).
- Un tiers déclare privilégier l’utilisation d’équipements (serveur, datacenter...) à haut rendement énergétique.
- 20% éteignent une imprimante collective  s’ils passent devant.
@ulend

@snap[south-west template-note text-gray]
L'optimisation dans les entreprises
@snapend

+++?image=assets/img/bg/orange.jpg
@title[Les grandes lignes de l'optimisation]
@ol[split-screen-list text-white](false)
- privilégier les appareils peu consommateurs (logique de terminal + GreenIT pour les entreprises)
- adopter les bons comportements (veille)
- s'impliquer dans les usages collectifs
- se poser des questions sur les usages
@olend

@snap[south-west template-note text-gray]
Les grandes lignes de l'optimisation
@snapend
