---?image=assets/img/pencils.jpg
@title[connaître ses appareils]

## @color[black](connaître ses appareils)

@fa[arrow-down text-black]


+++?image=assets/img/ghana.jpg&position=right&size=30% 100%
@title[Les déchets]
@snap[west span-70]
@ul[split-screen-list](false)
- plus de 50 millions de tonnes de déchets électriques et électroniques en 2020, soit plus de 7 kg par habitant et par an.
- A peine 20 % de ces déchets sont collectés et correctement recyclés. 
- Ces mines urbaines sont valorisées plus de 55 milliards de dollars (19 pour l’or, 15 pour le plastique, 10 pour le cuivre), soit un peu plus de 1 dollars par kg de déchet électronique.
- Les pays du sud émettent jusqu’à 15 fois moins de déchets, et le taux de réemploi dans ces pays est très élevé 
@ulend
@snapend

@snap[south-west template-note text-gray]
Source : GreenIT
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=47% 100%
@title[Choisir son type d'appareil]

@snap[west span-50]
![SNOWMAN](assets/img/data/consommation-annuelle-moyenne-appareil.png)
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Ordinateur personnel fixe (600 kWh/an)
- Ordinateur portable (150/300 kWh/an)
- Client léger (ex : [Wyse](https://www.dell.com/fr-fr/work/shop/points-de-terminaison-wyse-et-logiciels/sf/thin-clients))
- Terminal d'accès (Raspberry, NUC, Console de jeu, objets connectés)
- Smartphone
- Station de travail
@olend
@snapend

@snap[south-west template-note text-gray]
Les différentes catégories d'appareils
@snapend

+++?image=assets/img/bg/orange.jpg
@title[La tablette]

@snap[north-east span-20]
![EWASTE](assets/img/data/global-ewaste-monitor-2017-europe.jpg)
@snapend

@snap[west text-white span-80]
@ul[split-screen-list](false)
- L'Electric Power Research Institute (EPRI) estime la consommation électrique annuelle d’un iPad à 12 kWh. 
- 221 100 000 tablettes numériques vendues en 2013
- Selon les analyses d'IDC, les ventes mondiales de tablettes dépassé les ventes de PC de bureau en 2013, et devancé en 2014 celles des PC portables.
- les progrès réalisés par les microprocesseurs ne seront pas suffisants pour absorber l’augmentation du nombre de tablettes vendues et la gourmandise des nouveaux modèles. 
@ulend
@snapend

@snap[south-west template-note text-gray]
Le cas particulier de la tablette
@snapend

+++?image=assets/img/bg/yellow.jpg
@title[consommation et CO2]

@snap[west span-50]
@quote[1 kWh = 37,38g CO<sup>2</sup>](EDF)
@snapend

@snap[east span-50]
![portables](assets/img/data/consommation-ordinateurs.png)
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=68% 100%
@title[systeme exploitation]

@snap[west span-25 text-orange]
Qu'est-ce qu'un OS ?
@snapend

@snap[east span-75]
@ul[split-screen-list text-white](false)
- Pour chaque appareil, le système d'exploitation permet son utilisation
- Windows ; OSX / iOS ; Linux ; Android
- L'OS influe peu sur les ressources, mais davantage sur la dépendance et la sécurité
- En cas de conflit majeur, certains OS peuvent être bloqués, utilisés contre nous, ou inopérants sans réseau outre-atlantique
- capacités (faire tourner des jeux, logiciels pro) et contraintes (sécurité, périphériques connectables)
@ulend
@snapend

@snap[south-west template-note text-gray]
Le système d'exploitation et moi
@snapend


+++?image=assets/img/data/consommation-moyenne-appareils-menagers.png&size=contain
@title[Consommation moyenne des appareils ménagers]

@snap[south-west template-note text-gray]
Consommation moyenne des appareils ménagers
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=64% 100%
@title[les conclusions de l'observation]

@snap[west span-35 text-orange]
Les entreprises 
@snapend

@snap[east span-65]
@ul[split-screen-list text-white](false)
- 75%  des entreprises ne prêtent pas attention aux consommations de ressources (CPU, Disques, Mémoire) dans leurs développements ou achats de logiciels (Bureautique, Gestion, Solutions Métiers).
- Dans 95 % des organisations, les développeurs ne disposent ni d’outils, ni de méthodes pour les aider à consommer moins de ressources machines.
- Seulement un tiers favorise les développements ou l’achat de logiciels mettant en œuvre la répartition des tâches sur plusieurs serveurs
@ulend
@snapend

@snap[south-west template-note text-gray]
Au niveau des professionnels
@snapend
