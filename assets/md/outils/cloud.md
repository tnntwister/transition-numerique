---?image=assets/img/pencils.jpg
@title[Cloud ou pas cloud ?]

## @color[black](Cloud ou pas cloud ?)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg
@title[pollution cachée ?]

@snap[north text-white span-95]
Confiance dans les réseaux  =  <br>monopoliser des datacenters en leur imposant nos besoins de disponibilité
@snapend

@snap[south-west text-white span-50]
Avantages du cloud<br><br>
@ol[split-screen-list](false)
- sauvegarde 
- synchronisation
- ressources mutualisées
- lecture facilitée
@olend
@snapend

@snap[south-east text-white span-50]
Inconvénient du cloud<br><br>
@ol[split-screen-list](false)
- uptime des serveurs 
- disponibilité coûteuse en énergie
- coût financier
@olend
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=68% 100%
@title[risques du cloud]
@snap[west span-30]
Les risques
@snapend

@snap[east text-white span-70]
@ol[split-screen-list](false)
- risques pour la confidentialité des données
- risques juridiques liés à l’incertitude sur la localisation des données
- risques pour la disponibilité et l’intégrité des données
- risques liés à l’irréversibilité des contrats
@olend
@snapend

+++?image=assets/img/bg/pink.jpg&position=left&size=60% 100%
@title[avec qui partager ses données]
@snap[east text-pink span-40]
Avec qui<br>partager ses données ? 
@snapend

@snap[west text-white span-60]
@ul[split-screen-list](false)
- cloud d'un éditeur : pas d'intérêt à partager avec lui, lui voit son intérêt à ce que vous partagiez
- intranet professionnel : partage entre collègues, problème des données personnelles
- cloud familial : mutualisation bénéfique, synchronisation parfois délicate, maintenance à faire
- cloud personnel : synchronisation et maintenance à prix fort
@ulend
@snapend

+++
@title[résumé cloud]
Le cloud familial est la solution qui présente le plus de sécurité, d'indépendance et qui pousse à l'économie des ressources.