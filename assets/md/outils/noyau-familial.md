---?image=assets/img/pencils.jpg
@title[Créer un noyau familial]

## @color[black](Créer un noyau familial)

@fa[arrow-down text-black]

+++
@title[SaaS ou NAS]

@snap[west span-50]
SaaS 
office 365, iWork, Google docs...
@ol[split-screen-list](false)
- pas de maintenance
- coût long terme plus important
- limité en usage par un coût financier
- données non possédées
@olend
@snapend

@snap[east span-50]
NAS
@ol[split-screen-list](false)
- maîtrise de la machine
- surveillance plus lourde sur la sécurité
- nécessite plus de bagage technique
- peut être personnalisée
@olend
@snapend


+++?image=assets/img/data/synology_nas.jpg
@title[Exemple de NAS]
@snap[south-west template-note text-gray]
Exemple de NAS, marque Synology
@snapend
+++?image=assets/img/data/nas_accueil.jpg
@title[Interface d'un NAS synology]
@snap[south-west template-note text-gray]
Une interface similaire à un ordinateur
@snapend
+++?image=assets/img/data/nas_video.jpg
@title[gestion des contenus dans  un NAS synology]
@snap[south-west template-note text-gray]
Des contenus accessibles dans toute la maison
@snapend

+++?image=assets/img/data/nextcloud_apps_menu.png
@title[présentation de Nextcloud]
@snap[south-west template-note text-gray]
Nextcloud : un canevas d'applications
@snapend

+++?image=assets/img/data/nextcloud_client.png
@title[synchronisation de Nextcloud]
@snap[south-west template-note text-gray]
Une synchronisation similaire aux produits payants
@snapend

+++?image=assets/img/data/nextcloud_agenda.gif
@title[application de Nextcloud]
@snap[south-west template-note text-gray]
Tous les usages du quotidien
@snapend

+++
@title[Conclusion sur les outils]
@ul[split-screen-list](false)
- au niveau personnel, on a vu qu'il était préférable de mutualiser ses ressources et d'y accéder par des appareils peu gourmands en énergie
- on a vu qu'il fallait adapter ses besoins informatiques à ses besoins réels, et son matériel à ses besoins informatiques
- passons au niveau collectif, et voyons comment notre consommation se situe
@ulend
