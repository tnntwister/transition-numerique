---?image=assets/img/pencils.jpg
@title[ecologie]

## @color[black](l'écologie des données)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[écologie - sommaire]

@snap[west split-screen-heading text-orange span-50]
l'écologie des données
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- L'impact des données
- Les réseaux sociaux
- Consommer moins : quel sens en informatique ?
- Consommer local : quel sens en informatique ?
@olend
@snapend
