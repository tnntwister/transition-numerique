---?image=assets/img/phone.jpg
@title[Les réseaux sociaux]

## @color[white](Les réseaux sociaux)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Le réseau social : l'ennemi écologique]

@snap[west split-screen-heading text-orange span-50]
Le réseau social : l'ennemi écologique
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- temps de connexion maximum
- l'omniprésence du streaming
- une invitation à créer du contenu lourd et à le partager en permanence
- destruction des rapports réels
@olend
@snapend

+++?image=assets/img/data/reseaux-sociaux-utilisateurs-france-monde-infographie-juin-2018-tiz.jpg&size=contain
@title[Répartition des réseaux sociaux]

+++?image=assets/img/data/stats-globales-avril-2018.jpg&size=contain
@title[Mobilité et internet dans le monde]

+++?image=assets/img/data/ados-reseaux-sociaux-612x307.png&size=contain
@title[Les réseaux sociaux chez les jeunes]

+++?image=assets/img/data/facebook-stats.png&size=contain
@title[Facebook info]

+++?image=assets/img/data/global-mobile-video-trafic.png&size=contain
@title[Trafic vidéo]

+++?image=assets/img/data/number_of_photos_taken_worldwide.jpg&size=contain
@title[Nombre de photos]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Les réseaux sociaux et les jeunes]

@snap[west split-screen-heading text-orange span-50]
Les réseaux sociaux et les jeunes
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- [Le comportement des jeunes](https://www.meta-media.fr/2018/10/01/les-moins-de-13-ans-plebiscitent-toujours-snapchat-instagram-et-tiktok.html)
- les (très) jeunes délaissent les vrais rapports humains pour les rapports numériques- 
- snapchat et tiktok : des applications toxiques, très énergivores, où les prédateurs agissent beaucoup plus facilement.
- plus d'info : [le roi des rats](https://www.youtube.com/watch?v=2b54gMTgnaE)
@olend
@snapend
