---?image=assets/img/pencils.jpg
@title[Consommer local]

## @color[black](Consommer local<br>un sens en informatique ?)

@fa[arrow-down text-black]

+++
@title[la quadratue contre les gafam]

https://gafam.laquadrature.net/


+++?image=assets/img/bg/orange.jpg&position=right&size=57% 100%
@title[l'exemple des CHATONS]

@snap[west split-screen-heading text-orange span-45]
L'exemple CHATONS
@snapend

@snap[east text-white span-55]
@ul[split-screen-list](false)
- l'utilisation de logiciels libres
- aucun profilage publicitaire 
- le respect de vos données (droit d’accès, interopérabilité, non-transmission à des tiers) 
- la transparence 
- la neutralité (aucune surveillance ni censure en amont)  
- le chiffrement 
@ulend
@snapend

@snap[south-west template-note text-gray]
[Le collectif d'hébergeurs CHATONS](https://chatons.org/)
@snapend


+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[notre vie privée]

@snap[west split-screen-heading text-orange span-45]
La vie privée vs ?
@snapend

@snap[east text-white span-50]
@ul[split-screen-list](false)
- votre employeur actuel
- vos futurs employeurs 
- vos proches 
- le harcèlement 
- des escrocs / criminels
- gouvernance algorithmique
- de l’état 
@ulend
@snapend

@snap[south-west template-note text-gray]
La sphère la plus locale...
@snapend