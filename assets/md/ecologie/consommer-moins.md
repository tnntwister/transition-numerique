---?image=assets/img/pencils.jpg
@title[consommer moins : un sens en informatique ?]

## @color[black](Consommer moins : un sens en informatique ?)

@fa[arrow-down text-black]

+++?image=assets/img/bg/pink.jpg
@title[Les questions à se poser]
@snap[north-west text-white span-50]
__se poser les bonnes questions sur les applications qu'on utilise__
@snapend

@snap[south-west text-white span-50]
@ol[split-screen-list](false)
- est ce que je partage l'information ?
- est ce que je modifie l'information ? 
- est ce que l'information doit être conservée dans le temps ?
@olend
@snapend

@snap[north-east text-white span-50]
__s'interroger sur le divertissement__
@snapend

@snap[south-east text-white span-50]
@ol[split-screen-list](false)
- streaming vs téléchargement
- information vs détente
@olend
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[les conseils de tim ferriss]

@snap[west split-screen-heading text-orange span-50]
les conseils de Tim Ferriss
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- limiter les taches à l'important pour réduire le temps de travail (80/20)
- réduire le temps de travail pour limiter les tâches à l'important
- couper toutes les notifications
- vérifier votre BAL électronique 2 fois par jour
@olend
@snapend

+++
@title[citation catalogue]

@quote[au vu de la pollution générée par un ebook, l’utilisateur doit lire au moins 240 livres numériques en 3 ans avec le même appareil pour l’amortir écologiquement parlant.]

@quote[Non seulement les énormes catalogues nécessiteront de gros serveurs très gourmands en énergie, mais en plus les solutions « cloud computing »  dans lesquelles votre fichier sera stocké en ligne, et donc lisible depuis n’importe quel appareil, générera une consommation record. ]

@snap[south-west template-note text-gray]
[Source](https://editionnumerique.wordpress.com/2010/05/26/livre-electronique-ou-livre-papier-qui-est-le-plus-ecologique/)
@snapend


+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[vivre avec le dématérialisé]

@snap[west split-screen-heading text-orange span-50]
Vivre avec le dématérialisé
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- culture : attention à l'effet catalogue
- photos, vidéos personnelles : repasser par des supports physiques
- Limiter son temps de "streaming"
@olend
@snapend


+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[consommer moins pour les entreprises]

@snap[west split-screen-heading text-orange span-50]
pour les entreprises
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- 90 % favorisent l’utilisation de solutions de virtualisation sur leurs serveurs, en local ou hébergés.
- 70 % utilisent déjà des mécanismes de compression et/ou de déduplication de données.
- 65 % réutilisent des « vieux » serveurs obsolètes pour héberger des petites applications.
@olend
@snapend




