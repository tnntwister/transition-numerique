---?image=assets/img/phone.jpg
@title[impact]

## @color[white](l'impact des données)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=65% 100%
@title[les data centers]

@snap[west split-screen-heading text-orange span-30]
Les data centers
@snapend

@snap[east text-white span-70]
@ol[split-screen-list](false)
- 8,6 millions de datacenters dans le monde en 2017. [source IDC](http://www.businesswire.com/news/home/20141110005018/en/IDC-Finds-Growth-Consolidation-Changing-Ownership-Patterns)
- La climatisation et les systèmes de refroidissement représentent de 40 à 50 % de la consommation énergétique des data centers (Gimelec).
- Les data Centers américains ont consommé 91 milliards de kWh en 2013.
- les data Center représentent 4 % de la consommation énergétique mondiale en 2015, en croissance de 5 % par an.
@olend
@snapend

+++?image=assets/img/bg/black.jpg
@title[évolution des data centers]
@snap[middled text-white]
__Dans les cinq prochaines années, une majorité d'entreprises arrêteront de gérer leur propre infrastructure.__

@quote["Il faut arrêter de les faire tourner vingt-quatre heures sur vingt-quatre et sept jours sur sept même quand ils ne sont pas utilisés."]
@quote["Les serveurs appartiennent à nos clients. Ce sont eux qui décident quand et comment les employer"]
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=65% 100%
@title[les emails]

@snap[west split-screen-heading text-orange span-30]
Les emails
@snapend

@snap[east text-white span-70]
@ol[split-screen-list](false)
- 74 460 milliards d'e-mails ont été expédiés dans le monde en 2015 contre 66 795 milliards en 2013
- En 2015, 204 milliards de mails ont été envoyés dans le monde (hors-spam) chaque jour contre 183 milliards en 2013, soit environ 2 361 000 emails reçus et expédiés par seconde.
- Une entreprise de 100 personnes génère 13,6 t de CO2 chaque année ne serait-ce que par l'utilisation de l'e-mail, soit l'équivalent de 14 allers-retours Paris New York en avion. (Ademe)
@olend
@snapend

@snap[south-west template-note text-gray]
0.000003 grammes de CO² par message SMS
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=60% 100%
@title[les recherches internet]
@snap[west split-screen-heading text-orange span-40]
les recherches internet
@snapend
@snap[east text-white span-60]
@ol[split-screen-list](false)
- Wissner-Gross (Harvard)  : calcule que chaque requête sur Google produit 7g de C02 par la quantité d’énergie consommée par 500.000 serveurs. Il faut le multiplier par 200 millions de requêtes par jour [consommation du Laos].
- Selon les propres calculs de Google, il faut consommer 0.0003 kWh d’énergie pour faire une requête soit 1 kilojoule [autant d’énergie que ce qu’un corps humain adulte brûle en 10 min].
@olend
@snapend

@snap[south-west template-note text-gray]
Les recherches internet
@snapend