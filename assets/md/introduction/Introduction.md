---?image=assets/img/pencils.jpg
@title[Introduction]

## @color[black](Introduction)

@fa[arrow-down text-black]

+++?image=assets/img/moon.jpg&size=cover
@title[qu'est ce que la transition numérique ?]

@snap[east span-95 text-white]
@quote[Définir le besoin d'une transition numérique, c'est parler des risques liés à l'utilisation des objets du numérique]
@snapend

@snap[north-east template-note text-white]
Qu'est que la trandition numérique ?
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Identifier les risques]

@snap[west split-screen-heading text-orange span-50]
Identifier les risques
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Un risque en ressources (énergie, matières premières)
- Un risque de dépendance (incapacité à vivre sans informatique)
- Un risque sécuritaire (malveillance, coupure brutale, cadres hostiles)
@olend
@snapend

+++?image=assets/img/bg/pink.jpg&position=left&size=60% 100%
@title[Nos objectifs]

@snap[east split-screen-heading text-pink span-40]
Nos<br>objectifs
@snapend

@snap[west text-white span-60]
@ul[split-screen-list](false)
- Définir comment sélectionner nos outils informatiques
- Etudier l'impact de l'informatique sur l'environnement, et agir pour le réduire. 
- Imaginer un chemin vers plus de résilience
- Etre capable d'évoluer dans un environnement hostile
@ulend
@snapend

@snap[south-west template-note text-white]
Les conditions d'une transition réussie 
@snapend


+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Les chiffres]

@snap[west split-screen-byline text-orange]
Quelques sources...
@snapend

@snap[midpoint split-screen-img]
![DEVELOPER](assets/img/developer.jpg)
@snapend

@snap[east template-note text-gray link-gray]
@ul[split-screen-list](false)
- [Le livre blanc ADEME](https://www.ademe.fr/sites/default/files/assets/documents/livre-blanc-consommation-energetique-equipements-informatique-2015.pdf)
- [le planetoscope](https://www.planetoscope.com/)
- [Statista](https://www.statista.com/)
- [GreenIT.fr](https://www.greenit.fr)
- [les numeriques](https://www.lesnumeriques.com/)
@ulend
@snapend
