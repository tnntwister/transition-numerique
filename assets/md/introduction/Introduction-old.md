# Introduction
Définir le besoin d'une transition numérique, c'est parler des risques liés à l'utilisation des objets du numérique
- Un risque en ressources (énergie, matières premières)
- Un risque de dépendance
- Un risque sécuritaire
- Les conditions d'une transition réussie 

---- --------

Bonjour à toutes et à tous.

Cet atelier a pour objectif de nous faire réfléchir sur le thème de la transition numérique. Cela fait 15 ans que je développe avec des outils informatiques des sites internet et des programmes destinés à internet. Quand j'ai commencé à travailler, l'internet commercial connaissait ses balbutiements. Aujourd'hui, la plupart des biens se vendent par internet, l'administratif s'est virtualisé, les journaux et les lettres ont été remplacés par des notifications toujours plus rapides sur nos smartphones. Seulement voilà, est-ce que cette prise de poids de plus en plus importante du numérique dans nos vies n'est-elle pas en train de nous rendre obèses, incapables d'agir correctement sans nos appareils, notre wifi, nos mémoires partagées ?

Définir le besoin d'une transition numérique, c'est parler des risques liés à l'utilisation des objets du numérique. Aujourd'hui, nous parlons beaucoup de résilience des systèmes, c'est-à-dire de leur capacité à maintenir leur état malgré les chocs et les altérations possibles. Est-ce que la France peut continuer à maintenir son économie hexagonale si les banques font faillite par exemple. Est-ce qu'on peut continuer à faire tourner nos industries si le pétrole ou l'électricité deviennent plus rares ou plus chères ? Tous ces scénarios nous poussent à envisager des fonctionnements alternatifs de nos modes de vie actuels, de nous préparer au pire pour mieux passer les crises à venir. Quels problèmes pouvons-nous anticiper autour de notre informatique, et comment nous préparer à l'échelle individuelle à des évolutions du marché informatique non dictées par les lois commerciales ?

## Un risque en ressources (énergie, matières premières)

L'économie mondiale, tournée vers la big data et les échanges numériques, a un impact écologique qui augmente d'année en année. Selon International Data Corporation, le trafic mondial sera en 2020 de 44 zetaoctets, le nombre d'objets connectés sera de 80 milliards (2 pour la France). Cette activité a un impact environnemental important, estimé à 2% des émissions mondiales de gaz à effet de serre, soit autant que le transport aérien (source : GESI Smarter 2020). Néanmoins, seulement 20% des émissions sont imputables aux data centers, donc aux géants industriels du numérique. Le reste passe dans nos ordinateurs, nos smartphones et nos tablettes (47%), et nos réseaux de communication (28%).

A l'aube d'un scénario de pénurie pétrolière, lequel impactera forcément toutes les ressources liées à la production d'énergie, l'engagement des hébergeurs sur un uptime de 99%, soit un service garanti en permanence, pourrait bien un jour être revu à la baisse, soit pour des questions de coût financier, soit tout simplement par des lois de régulation de la consommation. Si l'électricité a cet avantage de pouvoir être acheté d'un pays à l'autre, l'entretien privatisé de nos lignes par ERDF nous pousse aussi à nous interroger sur la résilience de l'électricité publique.

Le facteur électrique n'est malheureusement la seule ressource qui peut nous empêcher d'utiliser nos chères machines ; ces dernières sont parmi les premières consommatrices des fameuses terres rares, 

## Un risque de dépendance
## Un risque sécuritaire
## Les conditions d'une transition réussie 
