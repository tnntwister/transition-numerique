---?image=assets/img/pencils.jpg
@title[Bonnes pratiques]

## @color[black](Bonnes pratiques)

@fa[arrow-down text-black]

+++
@title[bonnes pratiques]
@snap[west span-45]
@ul[split-screen-list](false)
- Choisir avec soin ses mots de passe
- Mettre à jour régulièrement vos logiciels
- Effectuer des sauvegardes régulières
- Sécuriser l’accès Wi-Fi 
- Être aussi prudent avec son ordiphone (smartphone) ou sa tablette qu’avec son ordinateur
@ulend
@snapend

@snap[east span-50]
@ul[split-screen-list](false)
- Protéger ses données lors de ses déplacements
- Être prudent lors de l’utilisation de sa messagerie
- Télécharger ses programmes sur les sites officiels des éditeurs
- Être vigilant lors d’un paiement sur Internet
- Séparer les usages personnels des usages professionnels
@ulend
@snapend

@snap[south-west template-note text-gray]
[Guide des bonnes pratiques](https://www.ssi.gouv.fr/uploads/2017/01/guide_cpme_bonnes_pratiques.pdf)
@snapend

+++
@title[mots de passe]

@ol[split-screen-list](false)
- La méthode phonétique : « J’ai acheté 5 CDs pour cent euros cet après-midi » :  ght5CDs%E7am ;
- La méthode des premières lettres : « Allons enfants de la patrie, le jour de gloire est arrivé » : aE2lP,lJ2Géa!
@olend

+++
@title[hygiène numérique]

@ul[split-screen-list](false)
- On ne se lave pas les mains juste pour éviter de tomber malade, mais aussi pour éviter de contaminer les autres. Il en va de même pour l’hygiène numérique.
- La surveillance de masse, qu’elle soit réelle ou supposée, nous pousse à l’auto-censure [&hellip;] et sclérose peu à peu le débat démocratique.
- Une bulle de filtres, c’est lorsque tous les sites que nous consultons détectent les informations qui nous plaisent et ne nous affichent plus que celles-ci. 
@ulend