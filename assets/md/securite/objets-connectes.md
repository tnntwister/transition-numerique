---?image=assets/img/pencils.jpg
@title[objets-connectes]

## @color[black](Objets connectés)

@fa[arrow-down text-black]

+++?image=assets/img/bg/pink.jpg
@title[objets-connectes définition]
@ul[text-white](false)
- Alors qu'Internet ne se prolonge habituellement pas au-delà du monde électronique, l'Internet des objets connectés représente les échanges de données provenant de dispositifs du monde réel avec le réseau Internet.
- Considéré comme le Web 3.0 qui fait suite à l'ère du Web social, l'Internet des objets revêt un caractère universel et désigne des objets connectés aux usages variés : e-santé, domotique, quantified self...
- Selon International Data Corporation, le nombre d'objets connectés sera de 80 milliards (2 pour la France)
@ulend

@snap[south-west template-note text-gray]
[Wikipedia](https://fr.wikipedia.org/wiki/Internet_des_objets)
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Shodan]
@snap[west split-screen-heading text-orange span-50]
Shodan
@snapend
@snap[east text-white span-45]
@ol[split-screen-list](false)
- Scan automatique des ports ouverts
- La sécurité n'est pas un impératif, donc négligée 
- un symptôme plus qu'une maladie
@olend
@snapend

@snap[south-west template-note text-gray]
[le site Shodan](https://www.shodan.io/)
@snapend

+++
@title[Légalisation faible sur les objets connectés]
@quote[les associations de protection demandent depuis longtemps aux institutions européennes d'imposer des exigences en matière de cybersécurité, comme des mises à jour de sécurité, des mots de passe forts ou du chiffrement pour les montres, les voitures connectées et autres réfrigérateurs intelligents.](ZDNet)

+++?image=assets/img/bg/orange.jpg&position=right&size=65% 100%
@title[recommandations de la CNIL]
@snap[west split-screen-heading text-orange span-30]
Que dit la CNIL ?
@snapend

@snap[east text-white span-70]
@ol[split-screen-list](false)
- vérifier a minima que l’objet ne permet pas à n’importe qui de s’y connecter :  appairage nécessitant un bouton d’accès physique ou l’usage d’un mot de passe
- changer le paramétrage par défaut de l’objet (mot de passe, code PIN, etc.)
- sécuriser l'accès par un mot de passe l’écran de déverrouillage du smartphone (ou de la tablette) et le réseau WiFi utilisé avec l’objet connecté
- éteindre l’objet quand il ne sert pas ou pour éviter de capter des données sensibles
@olend
@snapend


+++?image=assets/img/bg/orange.jpg&position=right&size=58% 100%
@title[veille sur les objets connectés]
@snap[west split-screen-heading text-orange span-40]
veille sur des objets connectés
@snapend

@snap[east text-white span-60]
@ol[split-screen-list](false)
- être d'autant plus vigilants sur les aspects de sécurisation lorsque les objets produisent des données sensibles, sur votre santé ou sur vos enfants ;
- être attentif concernant votre vie privée et celle des autres si vous associez l’objet à des réseaux sociaux, notamment en désactivant le partage automatique des données ;
- s'assurer de la possibilité d’accéder aux données et de les supprimer
@olend
@snapend

@snap[south-west template-note text-gray]
[la page de la CNIL](https://www.cnil.fr/fr/objets-connectes-noubliez-pas-de-les-securiser)
@snapend
