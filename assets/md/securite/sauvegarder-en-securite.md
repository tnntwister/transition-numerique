---?image=assets/img/pencils.jpg
@title[sauvegarder en sécurité]

## @color[black](Sauvegarder en sécurité)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[sauvegarder]

@snap[west split-screen-heading text-orange span-50]
Sauvegarder en sécurité
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Deux sauvegardes, une physique et une en réseau
- Les documents personnels ne devraient pas être sur un espace accessible à des inconnus.
- Utiliser des moyens mnémotechniques pour sortir des mots de passe trop simples.
@olend
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[portableApps]

@snap[west split-screen-heading text-orange span-50]
PortableApps
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Une clé USB sous Windows
- Votre navigateur garde vos données
- Toutes les applications libres sous Windows sont représentées
@olend
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=42% 100%
@title[portableApps]

@snap[west split-screen-heading text-orange span-55]
Synchronisation
@snapend

@snap[east text-white span-40]
@ol[split-screen-list](false)
- Synchronisation répertoire à répertoire
- Complète ou différentielle (Time Machine)
- Options de comparaison
@olend
@snapend