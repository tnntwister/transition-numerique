---?image=assets/img/pencils.jpg
@title[securite]

## @color[black](Sécurité)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[sécurité sommaire]

@snap[west split-screen-heading text-orange span-50]
Sécurité
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Bonnes pratiques
- Mon navigateur
- Sauvegarder en sécurité
- Les objets connectés
@olend
@snapend


