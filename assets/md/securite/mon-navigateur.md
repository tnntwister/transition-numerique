---?image=assets/img/pencils.jpg
@title[Mon navigateur]

## @color[black](Mon navigateur)

@fa[arrow-down text-black]

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[Mon navigateur]

@snap[west split-screen-heading text-orange span-50]
mon navigateur
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- tester les différents navigateurs, choisir le meilleur
- sélectionner les meilleures extensions
- synchroniser ses données ou non
@olend
@snapend

@snap[south-west template-note text-gray]
Un système dans le système
@snapend

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[mots de passe]

@snap[west split-screen-heading text-orange span-50]
Gérer ses mots de passe
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Utiliser un coffre de mots de passe (pratique **ou** sécure)
- Utiliser des mots de passe autogénérés
- Utiliser des moyens mnémotechniques pour sortir des mots de passe trop simples.
@olend
@snapend

+++
@title[partage sur internet]

Tout ce que vous mettez sur internet peut être récupéré. Le seul contenu qui n'est pas récupérable, c'est celui qu'on ne met pas.

+++?image=assets/img/bg/orange.jpg&position=right&size=50% 100%
@title[vie privée]

@snap[west split-screen-heading text-orange span-50]
Gérer sa vie privée
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Données de formulaire
- Outils anonymisants
- Navigation privée
@olend
@snapend

